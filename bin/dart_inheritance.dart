import 'dart:developer';
import 'dart:io';

//mixin
mixin Fuel {
  void fuel() {
    print('- with fuel ');
  } 
}

mixin LoadLimit {
  void loadLimitTwo() {
    print('- can sit for two');
  }
  void loadLimitSmall() {
    print('- can carry less people');
  }
  void loadLimitBig() {
    print('- can carry a lot of people');
  }
}

mixin Wheel {
  void wheel() {
    print('- with wheels');
  }
}
//abstract and with
abstract class Vehicle with Fuel, LoadLimit, Wheel {
  void vehicle() {
    print('Your Vehicle has ');
    fuel();
    wheel();
    loadLimitBig();
  }
}

abstract class Land with Wheel, Fuel, LoadLimit {
}

abstract class Air with Fuel, Wheel, LoadLimit {
}

abstract class Waterway with Fuel, LoadLimit {
}

////////////////////////////////////////////////
//extends
class Motorcycle extends Land{
  void motorcycle() {
    print("Motorcycle...");
    loadLimitTwo();
    wheel();
    fuel();
  }
}

class PrivateCar extends Land {
  void privateCar() {
    print("Private Car...");
    loadLimitSmall();
    wheel();
    fuel();
  }
}

class BusCar extends Land {
  void busCar() {
    print("Bus Car...");
    loadLimitBig();
    wheel();
    fuel();
  }
}

class JetSki extends Waterway{
  void jetSki() {
    print("Jet Ski...");
    loadLimitTwo();
    fuel();
  }
}
class SmallBoat extends Waterway {
  void smallBoat() {
    print("Small Boat...");
    loadLimitSmall();
    fuel();
  }
}

class CargoShip extends Waterway{
  void cargoShip() {
    print("Cargo Ship...");
    loadLimitBig();
    fuel();
  }
}

class JetPlane extends Air {
  void jetPlane() {
    print("Jet plane...");
    loadLimitTwo();
    wheel();
    fuel();
  }
}

class Helicopter extends Air {
  void helicopter() {
    print("Helicopter...");
    loadLimitSmall();
    wheel();
    fuel();
  }
}

class Airliner extends Air {
  void airliner() {
    print("Airliner...");
    wheel();
    fuel();
  }
}

  //implement
class Bicycle implements Vehicle {
  void bicycle(){
    vehicle();
    loadLimitTwo();
    fuel();
    wheel();
  }
  
  @override
  void fuel() {
    print("- no fuel required");
  }
  
  @override
  void loadLimitBig() {
    print("- can't carry a lot of passengers");
  }
  
  @override
  void loadLimitSmall() {
    print("- can't carry a lot of passengers");
  }
  
  @override
  void loadLimitTwo() {
    print("- can carry one or two people");
  }
  
  @override
  void vehicle() {
    print("Bicycle...");
  }
  
  @override
  void wheel() {
    print("- small rubber wheels");
  }
}
void main(List<String> args) {
  
}